#pragma config(Sensor, in3,    pent,           sensorPotentiometer)
#pragma config(Sensor, in2,    linefollower,   sensorLineFollower)
#pragma config(Motor,  port2,           mgr,           tmotorServoStandard, openLoop)
#pragma config(Motor,  port3,           dispensor,     tmotorVex393_MC29, openLoop)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//

#define NOTHING 0
#define WHITE 1
#define CLEAR 2
#define WOOD 3



int getType(int valueOfSensor);
void openLatch();
void setCup(int cup);
void closeLatch();
void nextCup();

task main()
{
//	int data[100];
//	int i = 0;
//	for(int j = 0; j < 100; j++) { //doesn't reset?
//		data[j] = 0;
//	}
	while(true) {
		switch(getType(SensorValue(linefollower))) {
		case NOTHING: //we just assume it's clear
			setCup(CLEAR);
			break;
		case WHITE:
			//set to white
			setCup(WHITE);
			break;
		case CLEAR:
			setCup(CLEAR);
			break;
		case WOOD:
			setCup(WOOD);
			break;
		}

		openLatch();
		wait1Msec(1);
		closeLatch();
		wait1Msec(1500);
//		data[i > 99? 0: i] = SensorValue(linefollower);
//		i++;
	}
}

void openLatch() {
	while(SensorValue(pent) < 2450) {
		motor[dispensor] = -50;
	}
	motor[dispensor] = 9;
}

void closeLatch() {
	while(SensorValue(pent) > 2300) {
		motor[dispensor] = 50;
	}
	motor[dispensor] = -9;
}
void setCup(int cup) {
	switch(cup) {
	case WHITE:
		motor[mgr] = 127;
		break;
	case CLEAR:
		motor[mgr] = 0;
		break;
	case WOOD:
		motor[mgr] = -127;
		break;
	}

	wait1Msec(500);//jack wants a little time for it to set
}
int getType(int valueOfSensor) {
	if(valueOfSensor > 2900) {
		return NOTHING;
	} else if (valueOfSensor > 2750) {
	  return CLEAR;
	} else if (valueOfSensor > 400) {
	  return WHITE;
	} else {
	  return WOOD;
	}
}
